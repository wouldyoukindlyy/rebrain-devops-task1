# Task no.1 

This project contains nginx.conf -- the default configuration file for NGINX.

## Getting Started And Ended 

This project does not need any instructions because there is nothing to deploy or test or anything else. 

## Built With

* [NGINX](https://www.nginx.com/) - The open source web server that powers more than 400 million websites

## Authors

* **Marie T.** - *Initial work* - [PurpleBooth](https://gitlab.rebrainme.com/unanoraked_at_gmail_com)

